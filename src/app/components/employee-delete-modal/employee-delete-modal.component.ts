import { Component, OnInit, ElementRef, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Employees, EmplyeesService } from '../../services/emplyees.service';
import { ModalComponent } from '../modal/modal.component';

declare const $;

@Component({
  selector: 'employee-delete-modal',
  templateUrl: './employee-delete-modal.component.html',
  styleUrls: ['./employee-delete-modal.component.css']
})
export class EmployeeDeleteModalComponent implements OnInit {

  @Input()
  employee:Employees;

  @Output()
  onDestroy:EventEmitter<Employees> = new EventEmitter<Employees>();

  @ViewChild(ModalComponent)
  modalComponent:ModalComponent;


  constructor(private element:ElementRef, private employeeService:EmplyeesService) { }

  ngOnInit() {
  }

  employeeDelete()
  {
    this.employeeService.deleteEmployee(this.employee);
    this.onDestroy.emit(this.employee); //Chama o metodo de call back do employs-list
    this.hide();
  }

  show()
  {    
    this.modalComponent.show()
  }
  
  hide()
  {
    this.modalComponent.hide();

  }


}