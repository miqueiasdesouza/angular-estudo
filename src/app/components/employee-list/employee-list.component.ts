import { Component, OnInit, ViewChild } from '@angular/core';
import { EmplyeesService, Employees } from '../../services/emplyees.service';
import { EmployeeNewModalComponent } from '../employee-new-modal/employee-new-modal.component';
import { EmployeeEditModalComponent } from '../employee-edit-modal/employee-edit-modal.component';
import { EmployeeDeleteModalComponent } from '../employee-delete-modal/employee-delete-modal.component';
import { AlertSuccessComponent } from '../alert-success/alert-success.component';

@Component({
  selector: 'employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  employees;
  showMessageSuccess = false;
  msg = {
    text: '',
    type: 'success'
  };
  
  employee = {};
  employeeToEdit:Employees;
  employeeToDelete:Employees;

  @ViewChild(EmployeeNewModalComponent)
  employeeNewModal:EmployeeNewModalComponent;
  
  @ViewChild(EmployeeEditModalComponent)
  employeeEditModal:EmployeeEditModalComponent;
  
  @ViewChild(EmployeeDeleteModalComponent)
  employeeDeleteModal:EmployeeDeleteModalComponent;  

  @ViewChild(AlertSuccessComponent)
  alert:AlertSuccessComponent;
  
  constructor(private employeeService:EmplyeesService) {
    this.employees = employeeService.getList();
  }

  ngOnInit() {
  }

  editModal(employee: Employees)
  {
    this.employeeToEdit = employee;
    this.employeeEditModal.show();
  }
  
  deleteModal(employee: Employees)
  {
    this.employeeToDelete = employee;
    this.employeeDeleteModal.show();
  }

  //Os metodos on são as chamads que fazemos para ser o retorno da ação que fizemos.
  //Nesse caso depois do nosso componente inserir o funcionario
  //Ele retorna para a listagem e chama essa função.
  //Esses parametros são inseridos nas views
  onEmployeeAdd(employee)
  {
    this.employee = employee;
    this.msg.text = `O Funcionário ${employee.name} foi iserido com sucesso`;    
    this.alert.show();
  }

  onEmployeeEdit(employee)
  {
    this.employee = employee;    
    this.msg.text = `O Funcionário ${employee.name} foi alterado com sucesso`;
    this.msg.type = 'info';
    this.alert.show();
  }

  onEmployeeDelete(employee)
  {        
    this.msg.text = `O Funcionário ${employee.name} foi excluido com sucesso`;
    this.msg.type = 'warning';
    this.alert.show();
  }

  fechou(event)
  {
    console.log('fechou', event);
  }
  
  abriu(event)
  {
    console.log('Abriu', event);
  }
}