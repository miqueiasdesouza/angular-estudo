import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'alert-success',
  templateUrl: './alert-success.component.html',
  styleUrls: ['./alert-success.component.css']
})
export class AlertSuccessComponent implements OnInit {

  @Input()
  type:string = 'success';
  
  timeOut:boolean = true;

  constructor() { }

  ngOnInit() {
  }

  show()
  {
    this.timeOut = false;

    setTimeout(() => {
      this.timeOut = true;
    }, 4000);

  }

}
