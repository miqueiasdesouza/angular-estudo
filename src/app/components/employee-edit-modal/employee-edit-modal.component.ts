import { Component, OnInit, ElementRef, Output, EventEmitter, Input, ViewChild } from '@angular/core';
import { Employees, EmplyeesService } from '../../services/emplyees.service'
import { ModalComponent } from '../modal/modal.component';

declare const $;

@Component({
  selector: 'employee-edit-modal',
  templateUrl: './employee-edit-modal.component.html',
  styleUrls: ['./employee-edit-modal.component.css']
})
export class EmployeeEditModalComponent implements OnInit {

  @Input()
  employees:Employees;

  @Output()
  onSubmit:EventEmitter<Employees> = new EventEmitter<Employees>();

  @ViewChild(ModalComponent)
  modalComponent:ModalComponent;
  
  constructor(private element: ElementRef) { 

    console.log(this.employees);
  }

  ngOnInit() {
  }


  editEmployee()
  {
    const copy = Object.assign([], this.employees);    
    this.onSubmit.emit(copy);
    this.hide();
  }

  hide()
  {
    this.modalComponent.hide()
  }
  
  show()
  {
    this.modalComponent.show();
  }


}
