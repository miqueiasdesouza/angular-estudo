import { Component, OnInit, ElementRef, Output, EventEmitter } from '@angular/core';

declare const $;

@Component({
  selector: 'modal',
  template:  `
  <div class="modal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <ng-content select="[modal-title]"></ng-content>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <ng-content></ng-content>              
      </div>
    </div>
  </div>
  `,
})
export class ModalComponent implements OnInit {

  @Output()
  onHide:EventEmitter<any> = new EventEmitter();
  
  @Output()
  onShow:EventEmitter<any> = new EventEmitter();

  constructor(private element:ElementRef) { }

  ngOnInit() {

    $(this.divModal).on('hidden.bs.modal', (e) => {
        //console.log('ok');
        this.onHide.emit(e);
    });
    
    $(this.divModal).on('shown.bs.modal', (e) => {
        this.onShow.emit(e);
    });

  }

  hide()
  {    
    $(this.divModal).modal('hide');
  }
  
  show()
  {    
    $(this.divModal).modal('show');
  }


  get divModal():HTMLElement
  {
    const nativeElement:HTMLElement = this.element.nativeElement;
    return nativeElement.firstChild as HTMLElement
  }

}
