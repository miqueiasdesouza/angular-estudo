import { Component, OnInit, ElementRef, Output, EventEmitter, ViewChild } from '@angular/core';
import { Employees, EmplyeesService } from '../../services/emplyees.service'
import { ModalComponent } from '../modal/modal.component';
import { Modalable } from '../modal/modalable';

declare const $;

@Component({
  selector: 'employee-new-modal',
  templateUrl: './employee-new-modal.component.html',
  styleUrls: ['./employee-new-modal.component.css']
})
export class EmployeeNewModalComponent extends Modalable implements OnInit {

  employees:Employees = {
    name:  '',
    salary: 0,
    bonus:  0
  }

  @ViewChild('inputName')
  inputName:ElementRef;

  @Output()
  onSubmit:EventEmitter<Employees> = new EventEmitter<Employees>();
  
  @Output()
  onHide:EventEmitter<Employees> = new EventEmitter<Employees>();

  
  constructor(private element: ElementRef,  private employeeService:EmplyeesService) { 
    super();
  }

  ngOnInit() {
    super.ngOnInit();

    this.onShow.subscribe(e => {
      this.inputName.nativeElement.focus();
    });
  }

  addEmployee()
  {
    const copy = Object.assign([], this.employees);
    this.employeeService.addEmploee(copy);
    this.onSubmit.emit(copy);
    this.hide();
  }
}
