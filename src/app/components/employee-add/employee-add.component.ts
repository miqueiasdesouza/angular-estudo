import { Component, OnInit } from '@angular/core';
import employees from '../../employees';
import { EmplyeesService } from '../../services/emplyees.service';

@Component({
  selector: 'employee-add',
  templateUrl: './employee-add.component.html',
  styleUrls: ['./employee-add.component.css']
})
export class EmployeeAddComponent implements OnInit {

  employees = {
    name:  '',
    salary: 0,
    bonus:  0
  }
  
  constructor(private employeeService:EmplyeesService) { 

  }

  ngOnInit() {
  }

  addEmployee()
  {
    const copy = Object.assign([], this.employees);  
    this.employeeService.addEmploee(copy);
  }

  


}
