import { Injectable } from '@angular/core';
import { StringMap } from '@angular/core/src/render3/jit/compiler_facade_interface';

export interface Employees {
  name:string,
  salary:number,
  bonus:number
}

@Injectable({
  providedIn: 'root'
})
export class EmplyeesService {
        
  employees:Employees[] = [];

  constructor() { }

  addEmploee(employee:Employees)
  {
    this.employees.push(employee);
  }

  getList()
  {
    return this.employees;
  }

  deleteEmployee(employee:Employees)
  {
    const index = this.employees.indexOf(employee);    
    this.employees.splice(index, 1);
    console.log(index);
    console.log(employee);
  }
}