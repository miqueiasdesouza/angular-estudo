import { Directive, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[salaryColor]'
})
export class SalaryColorDirective {

  constructor(private element:ElementRef) { 

    
    
  }

  @Input()
  set salaryColor(salary)
  {
    const nativeElement:HTMLElement = this.element.nativeElement;  
    const salaryInt = parseInt(salary);    
    nativeElement.style.color = ((salaryInt) >= 50 ? 'green' : 'red');
  }

}
